#!/bin/bash
        
for i in $( find weka -name \*.arff -print ); do
	echo item: $i
	java -classpath weka.jar weka.classifiers.bayes.NaiveBayes -o -t $i
done
