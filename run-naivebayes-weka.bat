@ECHO OFF
IF [%1]==[] (
	ECHO The results can be output to a different file by giving a commandline argument. Default is 'results.txt'.
	SET output-file=results.txt
) ELSE (
	SET output-file=%1
)

ECHO Outputting the results. Please wait for Weka to finish running.

IF EXIST %output-file% DEL %output-file%

FOR %%i IN ("weka\*.arff") DO (ECHO "%%i">>%output-file% & java -classpath weka.jar weka.classifiers.bayes.NaiveBayes -o -t %%i >> %output-file% & ECHO|set /p=". ")

ECHO .
ECHO Finished running Weka. The results are available at: %output-file%

PAUSE