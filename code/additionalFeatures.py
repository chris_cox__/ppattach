"""
Copyright (C) 2013-2015  University of Nebraska at Omaha NLP / KR Lab
Ben Susman <bsusman@unomaha.edu>, Yuliya Lierler <ylierler@unomaha.edu>, Dan Bailey

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
	
from features import Features

# The Zen of Python, by Tim Peters
# import this

class AdditionalFeatures(Features):
    def additionalFeature(self):
        """ Space for students to add their new feature(s) to PPattach.
        
        Note member variables can be accessed via the 'self' variable.
        See below for an example. Note that only the 'results' variable should be altered.
        
        Be creative and have fun!
        
        Implemented features can be found in features.py
        """  
        self.noun1
        self.noun2
        self.verb
        self.preposition

        self.results["additionalFeature"] = 'N' # Note appropriate values for the feature is '0'/'N'/'V'