#!/usr/bin/env python
# coding:utf-8

"""
Copyright (C) 2013-2015  University of Nebraska at Omaha NLP / KR Lab
Ben Susman <bsusman@unomaha.edu>, Yuliya Lierler <ylierler@unomaha.edu>, Dan Bailey

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
	
"PPattach"
#from code import features
#from code.features import Features

from additionalFeatures import AdditionalFeatures

__author__ = "University of Nebraska at Omaha NLP / KR Lab, Ben Susman <bsusman@unomaha.edu>, Yuliya Lierler <ylierler@unomaha.edu>, Dan Bailey"
__version__ = "1.0"

import sys
from summarizer import Summarizer
#from features import checkVerbNetFull, checkVerbNetHalf, deverbal, adverbal, phrasal, meronymsenses, relationship, instrument, similar
import nltk # @UnresolvedImport
import argparse
import os
import inspect

ONLY_WITH = False

DATA_PATH ="data"
TEST_DATA_PATH ="data/test"

WITH_SPECIFIC_FEATURES = set(["adverbal", "phrasal", "meronymsenses", "relationship", "instrument", "similar"])

'''convenience method for debugging'''
def seeFrameSyntax(vnclass):
    frames = vnclass.find("FRAMES")
    for frame in list(frames):
        partstring = ""
        parts = list(frame.find("SYNTAX"))
        for part in parts:
            try:
                partstring = partstring + " " + part.attrib['value']
            except:
                pass
        # print(partstring)


def getFeatureNames():
    method_names = [method[0] for method in inspect.getmembers(AdditionalFeatures, predicate=inspect.ismethod)]
    if '__repr__' in method_names:
        method_names.remove('__repr__')
    return method_names


def getPrepositions():
    rval = []
    if os.path.exists(DATA_PATH):
        rval = os.listdir(DATA_PATH)
    return rval
        
        
def main():
    feature_names = getFeatureNames()
    prepositions = getPrepositions()
    
    parser = argparse.ArgumentParser(description="Allows PPattach users to specify prepositions to test selected features on.\n" +
                                     "Example: ppattach -p all -f deverbal checkVerbNetFull")
    parser.add_argument("-p","--prepositions", help="Optional. Choose the prepositon(s) the features should be tested against.", 
                        default=prepositions, nargs="*", 
                        choices=prepositions)
    parser.add_argument("-f","--features", help="Optional. Choose the features(s) which should be included in the test.", 
                    default=feature_names, nargs="*", 
                    choices=feature_names)
    #parser.add_argument("-c","--classifier", help="Optional. Choose the classifier which will be used for machine learning.", 
    #            default="naive-bayes")
    parser.add_argument('--version', action='version', version='PPattach {version}'.format(version=__version__))
    
    args = parser.parse_args()
    run(args.prepositions, args.features)
    
def run(prepositionList, featureList):
    print(featureList)
    for sampleName in prepositionList:
        if sampleName == "with":
            reportSelectorList = ["generic", "generic-plus","with"]
        else:
            reportSelectorList = ["generic", "generic-plus"]
        
        for reportSelector in reportSelectorList:
            if reportSelector == "with":
                selectedFeatures = featureList
            elif reportSelector == "generic":
                selectedFeatures = []
            else:
                selectedFeatures = list(set(featureList) - WITH_SPECIFIC_FEATURES)
            
            print(selectedFeatures)
            
            with open(DATA_PATH + os.sep + sampleName,'r') as prepositionData:
                print(("Starting Feature Vector for Preposition:", sampleName, ", Report-type:" ,reportSelector, ", From file:",prepositionData.name, "..."))
                featureTests = list()
                for line in prepositionData:
                    #sys.stdout.write("Sentence: %s   \r" % (line.rstrip()) )
                    #sys.stdout.flush()
        
                    line = line.split(' ')  # take the input apart and assign         
                    
                    sentence = {"id"        : line[0],
                                "verb"      : line[1],
                                "noun1"     : line[2],
                                "prep"      : line[3],
                                "noun2"     : line[4],
                                "stdanswer" : line[5].rstrip()}
                    
                    featureTest = AdditionalFeatures(sentence)
                    
                    for selectedFeature in selectedFeatures:
                        # Calls each selected feature by string name
                        getattr(featureTest, selectedFeature)()

                    featureTests.append(featureTest)                        
                
                summary = Summarizer(selectedFeatures)
                summary.writereport(featureTests, sampleName, reportSelector)
                
                print(("... Finished Feature Vector for Preposition:", sampleName, ", Report-type:" ,reportSelector, ", From file:",prepositionData.name))

if __name__ == '__main__':   
    main()
 
