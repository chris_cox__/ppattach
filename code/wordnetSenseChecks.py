"""
Copyright (C) 2013-2015  University of Nebraska at Omaha NLP / KR Lab
Ben Susman <bsusman@unomaha.edu>, Yuliya Lierler <ylierler@unomaha.edu>, Dan Bailey

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

'''Checks the wordnet dictionary to see if words matching this sense contain
   a phrase.'''
def checkDefinition(sense, phrase):
    if sense.definition.rfind(phrase) > 0:
        return True
    else:
        return False


'''Checks the wordnet hierarchy to see if this sense is a hyponym of
   any of the senses listed in hypernymsList'''
def checkInheritance(sense, hypernymsList):
    if sense.name in hypernymsList:
        return True
    while True:
        try:
            sense = sense.hypernyms().pop()
            #if  hypernymsList[0] in ["instrumentality.n.03","act.n.02",'communication.n.02''body_part.n.01']:
            #    print sense
            # print '\t', sense, hypernymsList
        except:
            break
        if sense.name in hypernymsList:
            # print "RETURNED TRUE"
            return sense
    return False