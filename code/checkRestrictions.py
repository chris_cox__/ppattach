"""
Copyright (C) 2013-2015  University of Nebraska at Omaha NLP / KR Lab
Ben Susman <bsusman@unomaha.edu>, Yuliya Lierler <ylierler@unomaha.edu>, Dan Bailey

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from nltk.corpus import wordnet # @UnresolvedImport
from wordnetSenseChecks import checkDefinition, checkInheritance

HyperRestr = { 'concrete'     : ['physical_entity.n.01'],
               'abstract'     : ['abstraction.n.06'],
               'communication': ['communication.n.02'],
               'body_part'    : ['body_part.n.01'],
               'animate'      : ['causal_agent.n.01', 'living_thing.n.01'],
               'pointy'       : ['physical_entity.n.01'],
               'refl'         : ['physical_entity.n.01'],
               'solid'        : ['physical_entity.n.01'],
               'organization' : ['group.n.01'],
               'region'       : ['region.n.01'],
               'location'     : ['location.n.01', 'location.n.03'],
               'animal'       : ['animal.n.01'],
               'force'        : ['entity.n.01'],
               
               'Instrument'   : ["instrumentality.n.03", "act.n.02", 'communication.n.02', 'body_part.n.01'],
               'Theme'        : ['entity.n.01'],
               'Actor'        : ['causal_agent.n.01'],
               'Patient'      : ['entity.n.01'],
               'Recipient'    : ['entity.n.01'],
               'Oblique'      : ['entity.n.01'],
               'Location'     : ['location.n.01', 'location.n.03'],
               'Destination'  : ['entity.n.01'],
               'Experiencer'  : ['entity.n.01'],
               'Source'       : ['entity.n.01'],
               'Beneficiary'  : ['entity.n.01'],
               'Agent'        : ['entity.n.01'],
               'Product'      : ['entity.n.01'],
               'Material'     : ['entity.n.01'],
               'Topic'        : ['entity.n.01'],
               'Predicate'    : ['entity.n.01'],
               'Asset'        : ['entity.n.01'],
               
               'Extent'       : ['entity.n.01'],
               'Proposition'  : ['entity.n.01'],
               'Cause'        : ['entity.n.01'],
               'Value'        : ['entity.n.01'],
               'currency'     : ['currency.n.01'],
               'Attribute'    : ['attribute.n.02'],
               'machine'      : ['machine.n.01'],
               'scalar'       : ['scalar.n.01'],
               'Stimulus'     : ['stimulation.n.02'],
               'comestible'   : ['comestible.n.01'],
                }

DictRestr = { 'concrete'    : 'return false',
             'abstract'     : 'return false',
             'communication': 'return false',
             'body_part'    : 'return false',
             'animate'      : 'return false',
             'pointy'       : 'sharp',
             'refl'         : 'return false',
             'solid'        : 'return false',
             'organization' : 'return false',
             'region'       : 'return false',
             'location'     : 'return false',
             'animal'       : 'return false',
             'force'        : 'return false',
             
             'Instrument'   : 'used ',
             'Theme'        : 'return false',
             'Actor'        : 'return false',
             'Patient'      : 'return false',
             'Oblique'      : 'return false',
             'Recipient'    : 'return false',
             'Location'     : 'return false',
             'Destination'  : 'return false',
             'Experiencer'  : 'return false',
             'Source'       : 'return false',
             'Beneficiary'  : 'return false',
             'Agent'        : 'return false',
             'Product'      : 'return false',
             'Material'     : 'return false',
             'Topic'        : 'return false',
             'Predicate'    : 'return false',
             'Asset'        : 'return false',
             
             'Extent'       : 'return false',
             'Proposition'  : 'return false',
             'Cause'        : 'return false',
             'Value'        : 'return false',
             'currency'     : 'return false',
             'Attribute'    : 'return false',
             'scalar'       : 'return false',
             'machine'      : 'return false',
             'Stimulus'     : 'return false',
             'comestible'   : 'return false',
             }

''' A class to encapsulate the logic of matching word triples (verb, noun, noun) or pairs (verb, noun)
    with VerbNet classframes.'''
class VNClassFramesWith:
    '''to initialize, it requires a verbnet class and an idea of how many nouns we will compare. The
       quantity of nouns decides whether we match with frames such as: V noun {with} noun
       or V {with} noun. The logic will compile the restrictions relevant for the nouns in such a frame
       and maintain a self.frames field for those restrictions as well as a self.classid identifying the frame.'''
    def __init__(self, vnclass, numOfNouns, prep):
        if(numOfNouns == 1):
            priorTag = 'VERB'
        elif(numOfNouns == 2):
            priorTag = 'NP'
        else: raise ValueError
        self.frames = list()
        self.classid = vnclass.attrib['ID']
        frames = vnclass.find("FRAMES")
        for frame in list(frames):
            parts = list(frame.find("SYNTAX"))
            for total in range(0, len(parts) - 1):
                if parts[total].tag == 'PREP' and parts[total - 1].tag == priorTag:
                    if 'value' in list(parts[total].keys()) and \
                      parts[total].attrib['value'].rfind(prep) >= 0:
                        if 'value' in list(parts[total + 1].keys()) and numOfNouns == 2:
                            firstTheme = parts[total - 1].attrib['value']
                            firstSelRest = getSelectionalRestr(vnclass, firstTheme)
                            secondTheme = parts[total + 1].attrib['value']
                            secondSelRest = getSelectionalRestr(vnclass, secondTheme)
                            #if firstTheme == "Patient" or secondTheme == "Patient":
                            #    print prep, firstTheme, firstSelRest, secondTheme, secondSelRest
                            self.frames.append({'Theme1'    : firstTheme,
                                                 'SelRestr1' : firstSelRest,
                                                 'Theme2'    : secondTheme,
                                                 'SelRestr2' : secondSelRest})
                        elif 'value' in list(parts[total + 1].keys()):
                            theme = parts[total + 1].attrib['value']
                            selRest = getSelectionalRestr(vnclass, theme)
                            self.frames.append({'Theme1'    : theme,
                                                 'SelRestr1' : selRest})
    '''Compare a specific set of nouns to the restrictions found in the initializer'''
    def compareNouns(self, noun1, noun2=None):
        results = list()
        for frame in self.frames:
            results1 = checkRestrictions(noun1, frame['Theme1'], frame['SelRestr1'])
            if noun2:
                results2 = checkRestrictions(noun2, frame['Theme2'], frame['SelRestr2'])
            else:
                results2 = None
            result = None
            if results1 and (results2 or not noun2):
                result = {'VerbNetClass': self.classid,
                          'Noun1Theme'  : frame['Theme1'],
                          'Noun1Senses' : results1}
                if results2:
                    result.update({'Noun2Theme'  : frame['Theme1'],
                                   'Noun2Senses' : results2})
                results.append(result)
            
        return results
    

'''finds the selectional restrictions associated with a thematic role
   in a verbnet class.'''
def getSelectionalRestr(vnclass, thematicRole):     
    selectional = list()
    logic = ''
    for themrole in vnclass.find('THEMROLES'):
            if thematicRole == themrole.attrib['type']:
                restrs = themrole.find('SELRESTRS')
                if list(restrs.keys()):
                    logic = restrs.attrib['logic']
                for selrestr in themrole.find('SELRESTRS'):
                    try: 
                        selectional.insert(0, (selrestr.attrib['type'], selrestr.attrib['Value']))
                    except:
                        pass
    return {'logic': logic, 'restr':selectional}
    
    
'''compares a noun to the rules governing the thematic roles and selectional restrictions defined
   dictionary at the top of this file'''
def checkRestrictions(noun, thematic, selectional):
    matchingsenses = set()
    thematicmatch = False
    selectionalmatch = True
    
    synsets = wordnet.synsets(noun, pos=wordnet.NOUN)
    # Added more synsets to deal with specific sets of nouns better
    if noun.isdigit() and len(noun) == 4:
        synsets = [wordnet.synset('year.n.01'),
                   wordnet.synset('year.n.02'),
                   wordnet.synset('year.n.03')]
    elif str(noun.replace(',','')).isnumeric():
        synsets = [wordnet.synset('definite_quantity.n.01')]
    elif noun == '%':
        synsets = [wordnet.synset('ratio.n.01'),
                   wordnet.synset('ratio.n.02')]
    elif isNamedEntity(noun):
        synsets = [wordnet.synset('person.n.01'),
                   wordnet.synset('organization.n.01'),
                   wordnet.synset('geographical_area.n.01')]
        
    for sense in synsets:
        if checkInheritance(sense, HyperRestr[thematic.rstrip('12')])\
           or checkDefinition(sense, DictRestr[thematic.rstrip('12')]):
            thematicmatch = True    
            
        if len(selectional['restr']) > 0 : 
            selectionalmatch = None
            for restriction in selectional['restr']:
                inheritanceChecks = HyperRestr[restriction[0]]
                dictionaryCheck = DictRestr[restriction[0]] 
                thismatch = checkInheritance(sense, inheritanceChecks) \
                            or checkDefinition(sense, dictionaryCheck)
                if not thismatch and restriction[1] == '-':
                    thismatch = True
                elif thismatch and restriction[1] == '+':
                    thismatch = True
                else: thismatch = False
                if selectionalmatch == None: 
                    selectionalmatch = thismatch
                elif selectional['logic'] == 'or':
                    selectionalmatch = selectionalmatch or thismatch
                else:
                    selectionalmatch = selectionalmatch and thismatch
                
        if selectionalmatch and thematicmatch:
            matchingsenses.add(sense)
    return matchingsenses


'''Does naive testing of the noun to determine if we will consider it a
   named entity.'''
def isNamedEntity(noun):
    senses = wordnet.synsets(noun)
    if not senses:
        return True  # If it's not in WordNet, assume that it's a Named Entity
    elif len(senses) == 1 and not senses[0].hypernyms():
        return True  # Only 1 sense that isn't in the hierarchy. 
                    # Just like Czechoslovakia.
    return False
