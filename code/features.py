"""
Copyright (C) 2013-2015  University of Nebraska at Omaha NLP / KR Lab
Ben Susman <bsusman@unomaha.edu>, Yuliya Lierler <ylierler@unomaha.edu>, Dan Bailey

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
	
import meronymy  # @UnresolvedImport
from checkRestrictions import VNClassFramesWith
from wordnetSenseChecks import checkDefinition, checkInheritance
from nltk.corpus import verbnet, wordnet # @UnresolvedImport
from nltk.stem.wordnet import WordNetLemmatizer # @UnresolvedImport
from nltk.stem import PorterStemmer # @UnresolvedImport

import copy, re

Pronouns = { 'another'  : 'thing',
             'it'       : 'thing',
             'her'      : 'person',
             'him'      : 'person',
             'them'     : 'group'}

''' Not a feature.
   takes in a set of verbnet classids and returns a set that includes
   those same verbnet classids together with all of their inherited
   hypernyms'''
def verbNetSuperclasses(vnclassids):
    superclassids = copy.copy(vnclassids)
    for classid in vnclassids:
        while(re.search('[0-9]-[0-9]$', classid)) :
            classid = classid[0:len(classid) - 2]
            superclassids.insert(0, classid)
    return superclassids

class Features:    
    def __init__(self, sentence):
        self.sentence = sentence

        lmtzr = WordNetLemmatizer()
        self.verb = lmtzr.lemmatize(sentence['verb'].lower(), 'v')
        self.noun1 = sentence['noun1']
        self.preposition = sentence['prep']
        self.noun2 = sentence['noun2']
        self.answer = sentence['stdanswer']
        self.results = dict()

        classids = verbnet.classids(lemma=self.verb)
        superclass = verbNetSuperclasses(classids)
        if superclass == []:
            self.verbclass = self.verb
        else:
            self.verbclass = ",".join(superclass)
        
        self.verbclass = '"'+self.verbclass+'"'
    
    def __str__(self):        
        return "sentence {0}; results: {1}".format(str(self.sentence), str(self.results)) 
        
            
    def verbNetFull(self):
        lmtzr = WordNetLemmatizer()
        
        noun = lmtzr.lemmatize(self.noun1.lower(), 'n')
        if noun in Pronouns: noun = Pronouns[noun]
        noun2 = lmtzr.lemmatize(self.noun2.lower(), 'n')
        if noun2 in Pronouns: noun2 = Pronouns[noun2]
        verb = lmtzr.lemmatize(self.verb.lower(), 'v')
        
        prepUsesResult = list()
        classids = verbnet.classids(lemma=verb)
        superclassids = verbNetSuperclasses(classids)
        for classid in superclassids:
            
            ###############################
            #####DEBUG REMOVE REMOVE REMOVE
            if classid == 'slide-11.2-1':
                self.results["verbNetFull"] = '0'
                return 
            ################################
            ################################
            
            vnclass = verbnet.vnclass(classid)
            verbnetframeswith = VNClassFramesWith(vnclass, 2, self.preposition)
            prepUsesResult.extend(verbnetframeswith.compareNouns(noun, noun2))
        
        if prepUsesResult:    
            self.results["verbNetFull"] = 'V'
            return prepUsesResult
        self.results["verbNetFull"] = '0'
        
        
    '''Determines if verb, noun, noun2 fit in any verbnet frames according to the thematic roles and
       selectional restrictions involved. This only considers verbnet matches which provide a frame
       syntax of style: (a) Verb Noun {with} Noun2  or (b) Verb {with} Noun.
       The output is a dictionary such as:
          {VerbNetClass: <the verbnet class that allows this match and includes the given verb>
           Noun1Theme: <the theme that the first noun matches>
           Noun1Senses: <the senses that include the first noun and match the frame/restrictions>
           Noun2Theme: <the theme that the second noun matches>
           Noun2Senses: <the senses that include the second noun and match the frame/restrictions>}'''
    def verbNetHalf(self):
        lmtzr = WordNetLemmatizer()
        
        noun = lmtzr.lemmatize(self.noun2.lower(), 'n')
        if noun in Pronouns: noun = Pronouns[noun]
        verb = lmtzr.lemmatize(self.verb.lower(), 'v')
        
        prepUsesResult = list()
        classids = verbnet.classids(lemma=verb)
        superclassids = verbNetSuperclasses(classids)
        for classid in superclassids:
            
            ######################################
            ## Diagonose Potential System Error ##
            if classid == 'slide-11.2-1':
                self.results["verbNetHalf"] = '0'
                return 
            ######################################
            ######################################
            
            vnclass = verbnet.vnclass(classid)        
            verbnetframeswith = VNClassFramesWith(vnclass, 1, self.preposition)
            prepUsesResult.extend(verbnetframeswith.compareNouns(noun))
        if prepUsesResult:
            self.results["verbNetHalf"] = 'V'
            return prepUsesResult
        self.results["verbNetHalf"] = '0'
    
    
    '''Decides whether a noun1 is acting as a verb in a nominalization sense. First, it is necessary to 
       identify if verbs are derivationally related to this noun1. Then, we can check noun2 to see
       if it matches the verbnet selectional restrictions that would be in play if noun1 were turned 
       into a verb and the syntax frame Verb {with} Noun2 was being employed.'''
    def nominalization(self):
        noun1 = self.noun1
        noun2 = self.noun2
        verbs = list()
        for sense in wordnet.synsets(noun1, pos=wordnet.NOUN):
            for lemma in sense.lemmas:
                if lemma.name[0:3] == noun1[0:3]:
                    forms = lemma.derivationally_related_forms()
                    for form in forms:
                        if form.synset.pos == wordnet.VERB:
                            for lemma in form.synset.lemmas:
                                lemmakey = lemma.key.rstrip(':')
                                verbs.extend(verbnet.classids(wordnetid=lemmakey))
        
        # nominalization = False
        verbs = verbNetSuperclasses(verbs)
        for verb in verbs:
            try:
                frame = VNClassFramesWith(verbnet.vnclass(verb), 1, self.preposition)
                # pp.pprint( frame.frames )
                if frame.compareNouns(noun2):
                    # print line
                    # print frame.classid
                    # print verb
                    # nominalization = True
                    self.results["nominalization"] = 'N'
                    return verb, frame.frames
            except:
                if not verb == "slide-11.2-1":
                    print(("Verbnet class not found", verb))
        self.results["nominalization"] = '0'
          
    
    ''' Currently unused. Identifies if the verb is a form of to be. ''' 
    """   
    def be(self):
        '''forms of the verb, to Be'''
        beForms = ['is', 'was', 'be', '\'s', 'am', 'are', 'been']
        if self.verb in beForms:
            self.results["be"] = 'N'
            return True
        self.results["be"] = '0'
    """
        
    
    ''' Currently unused. Identifies if the verb is a form of to do. '''        
    """
    def do(self):
        '''forms of the verb, to Do'''
        doForms = ['do','doing','did']
        if self.verb in doForms:
            self.results["do"] = 'V'
            return True
        self.results["do"] = '0'
    """
	
	
    '''Checks to see if a noun fits our criteria for being an instrument.
       This version is broken out to allow identification of useful/helpful
       criteria.'''
    def instrumentality(self):
        nounsenses = wordnet.synsets(self.noun2)
        aflag = iflag = bflag = cflag = uflag = tflag = False
        instrsenses = set()
        for sense in nounsenses:
            check = checkInheritance(sense, ["instrumentality.n.03"])
            iflag = iflag or check
            check = checkInheritance(sense, ["act.n.02"])
            aflag = aflag or check
            check = checkInheritance(sense, ['communication.n.02'])
            cflag = cflag or check
            check = checkInheritance(sense, ['body_part.n.01'])
            bflag = bflag or check
            # misses sentences beginning with 'used'
            check = checkDefinition(sense, ' used ')
            uflag = uflag or check
            tflag = iflag or aflag or bflag or cflag or uflag
            if tflag:
                instrsenses.add(sense)
                
        if instrsenses:
            self.results["instrumentality"] = 'V'
            return instrsenses
        self.results["instrumentality"] = '0'
      
    
    def adverbalUse(self):
        noun = self.noun2
        adverbalsenses = set()
        for lemma in wordnet.lemmas(noun):
            if lemma.name == noun:
                adjs = lemma.derivationally_related_forms()
                for adj in adjs:
                    if adj.synset.pos == wordnet.ADJ:
                        # for lemma in adj.synset.lemmas:
                            advs = wordnet.synsets(adj.name + 'ly')
                            for adv in advs:
                                adverbalsenses.add(adv)
    #     for sense in wordnet.synsets( noun ):
    #         if checkInheritance( sense, ["property.n.02"]):
    #             adverbalsenses.add( sense )
        if adverbalsenses:
            self.results["adverbalUse"] = 'V'
            return adverbalsenses
        self.results["adverbalUse"] = '0'
        
            
    '''tests the similarity of the two nouns according to the  Leacock and Chodorow
       similarity algorithm. If the similarity is greater that 2, it returns the
       most similar pair of senses for the two nouns.'''
    def similarity(self):
            noun1senses = wordnet.synsets(self.noun1, pos=wordnet.NOUN)
            noun2senses = wordnet.synsets(self.noun2, pos=wordnet.NOUN)
             
            lch = 0
            for noun1sense in noun1senses:
                for noun2sense in noun2senses:
                    temp = noun1sense.lch_similarity(noun2sense)
                    if temp > lch:
                        similarsenses = (noun1sense, noun2sense)
                        lch = temp
            if lch > 2.0:
                self.results["similarity"] = 'N'
                return similarsenses
            else:
                self.results["similarity"] = '0'
                return False

        
    def meronymy(self):
        noun = self.noun1
        noun2 = self.noun2
         
        if meronymy.meronymsenses(noun, noun2, meronymy.part):
            self.results["meronymy"] = 'N' 
        elif meronymy.meronymsenses(noun, noun2, meronymy.member):
            self.results["meronymy"] = 'N'
        elif meronymy.meronymsenses(noun, noun2, meronymy.substance):
            self.results["meronymy"] = 'N'
        else:
            self.results["meronymy"] = '0'


    def relationalNoun( self ):
        for sense in wordnet.synsets(self.noun1, pos=wordnet.NOUN):
            if checkInheritance( sense, ["relationship.n.01", "relationship.n.02",\
                                         "relationship.n.03","relationship.n.04"]):
                self.results["relationship"] = 'N'
                return sense 
        self.results["relationship"] = '0'
		

    '''Checks whether the two words, when put together, make a common verbal phrase
       according to wordnet.'''
    def idiom(self):
        ps = PorterStemmer() 
        phrase = ps.stem(self.verb) + '_' + self.noun1
        phrasesense = wordnet.synsets(phrase)
        if phrasesense:
            self.results["idiom"] = 'N'
            return phrasesense
        self.results["idiom"] = '0'